import { Injectable } from "@angular/core";

import { Item } from "../../models/item";
// import { AngularFireDatabase } from "angularfire2/database";
import { AngularFirestore } from "angularfire2/firestore";
// import { Observable } from "rxjs/Observable";

@Injectable()
export class Items {
  items: Item[] = [];
  // items: Observable<any[]>;
  // items: Observable<Item>;
  defaultItem: any = {
    name: "Burt Bear",
    profilePic: "assets/img/speakers/bear.jpg",
    about: "Burt is a Bear."
  };

  constructor(private afs: AngularFirestore) {
    this.getList()
      .then(usersCollection => {
        usersCollection.valueChanges().subscribe(
          userList => {
            console.log("items", userList);
            userList.forEach(user => {
              this.items.push(new Item(user));
            });
          },
          error => {
            console.log(error);
          }
        );
      })
      .catch(err => {
        console.log(err);
      });
    // .then(res => {
    //   this.items = res;
    //   console.log("test", res);
    // })
    // .catch(err => {
    //   console.log("error", err);
    // });
    // let items = [
    // {
    //   "name": "Burt Bear",
    //   "profilePic": "assets/img/speakers/bear.jpg",
    //   "about": "Burt is a Bear."
    // },
    // {
    //   "name": "Charlie Cheetah",
    //   "profilePic": "assets/img/speakers/cheetah.jpg",
    //   "about": "Charlie is a Cheetah."
    // },
    // {
    //   "name": "Donald Duck",
    //   "profilePic": "assets/img/speakers/duck.jpg",
    //   "about": "Donald is a Duck."
    // },
    // {
    //   "name": "Eva Eagle",
    //   "profilePic": "assets/img/speakers/eagle.jpg",
    //   "about": "Eva is an Eagle."
    // },
    // {
    //   "name": "Ellie Elephant",
    //   "profilePic": "assets/img/speakers/elephant.jpg",
    //   "about": "Ellie is an Elephant."
    // },
    // {
    //   "name": "Molly Mouse",
    //   "profilePic": "assets/img/speakers/mouse.jpg",
    //   "about": "Molly is a Mouse."
    // },
    // {
    //   "name": "Paul Puppy",
    //   "profilePic": "assets/img/speakers/puppy.jpg",
    //   "about": "Paul is a Puppy."
    // }
    // ];

    // for (let item of items) {
    // this.items.push(new Item(item));
    // }
  }

  async getList() {
    let usersCollection = await this.afs.collection("users");
    return usersCollection;
  }

  query(params?: any) {
    if (!params) {
      return this.items ? this.items : [this.defaultItem];
    }

    return this.items.filter(item => {
      for (let key in params) {
        let field = item[key];
        if (
          typeof field == "string" &&
          field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0
        ) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: Item) {
    // this.items.push(item);
  }

  delete(item: Item) {
    // this.items.splice(this.items.indexOf(item), 1);
  }
}
