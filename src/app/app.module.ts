import { HttpClient, HttpClientModule } from "@angular/common/http";
import { ErrorHandler, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { Camera } from "@ionic-native/camera";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { IonicStorageModule, Storage } from "@ionic/storage";
import { GooglePlus } from "@ionic-native/google-plus";
import { Diagnostic } from '@ionic-native/diagnostic';

import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";

import { Settings } from "../providers/providers";
import { Items } from "../mocks/providers/items";
import { FileTransfer } from "../providers/providers";
import { User } from "../providers/providers";
import { Api } from "../providers/providers";
import { MyApp } from "./app.component";

// firabase
import { AngularFireModule } from "angularfire2";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase
} from "angularfire2/database";
import {
  AngularFirestore,
  AngularFirestoreModule
} from "angularfire2/firestore";

import { AngularFireAuth, AngularFireAuthModule } from "angularfire2/auth";

export const firebaseConfig = {
  apiKey: "AIzaSyCtP3JperUP3CoIVnJLarELnZ4KtURtXdo",
  authDomain: "ionic-pwa-sts.firebaseapp.com",
  databaseURL: "https://ionic-pwa-sts.firebaseio.com",
  projectId: "ionic-pwa-sts",
  storageBucket: "ionic-pwa-sts.appspot.com",
  messagingSenderId: "345263253026"
};

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: "Ionitron J. Framework",
    option3: "3",
    option4: "Hello"
  });
}

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFirestoreModule.enablePersistence()
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    Api,
    User,
    Items,
    Camera,
    StatusBar,
    GooglePlus,
    Diagnostic,
    SplashScreen,
    FileTransfer,
    AngularFireAuth,
    AngularFirestore,
    AngularFireDatabase,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
