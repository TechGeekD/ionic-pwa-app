import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { Items } from '../../providers/items/items';

import { ItemCreatePage } from './item-create';
import { Diagnostic } from '@ionic-native/diagnostic';
@NgModule({
  declarations: [
    ItemCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(ItemCreatePage),
    TranslateModule.forChild()
  ],
  exports: [
    ItemCreatePage
  ],
  providers: [
    Items,
    Diagnostic
  ]
})
export class ItemCreatePageModule { }
