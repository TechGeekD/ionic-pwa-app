import { Component, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Camera } from "@ionic-native/camera";
import { IonicPage, NavController, ViewController } from "ionic-angular";
import { Items } from "../../providers/items/items";
import { Diagnostic } from "@ionic-native/diagnostic";

@IonicPage()
@Component({
  selector: "page-item-create",
  templateUrl: "item-create.html"
})
export class ItemCreatePage {
  @ViewChild("fileInput") fileInput;

  isReadyToSave: boolean;

  item: any;

  form: FormGroup;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    formBuilder: FormBuilder,
    public camera: Camera,
    private items: Items,
    private diagnostic: Diagnostic
  ) {
    this.form = formBuilder.group({
      profilePic: [""],
      name: ["", Validators.required],
      about: [""]
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe(v => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ionViewDidLoad() {}

  getPicture() {
    // this.diagnostic.isCameraAuthorized().then(authorized => {
    //   if (authorized) takePic();
    //   else {
    //     this.diagnostic.requestCameraAuthorization().then(status => {
    //       if (status == this.diagnostic.permissionStatus.GRANTED) takePic();
    //       else {
    //         // Permissions not granted
    //         // Therefore, create and present toast
    //         alert("Permission Not Granted");
    //       }
    //     });
    //   }
    // });
    // let takePic = () => {
    let successCallback = isAvailable => {
      console.log("Is available? " + isAvailable);
      this.camera
        .getPicture({
          destinationType: this.camera.DestinationType.DATA_URL,
          targetWidth: 96,
          targetHeight: 96
        })
        .then(
          data => {
            this.form.patchValue({
              profilePic: {
                base64: "data:image/jpg;base64," + data,
                type: "jpg",
                name: new Date().getTime() + ".jpg"
              }
            });
          },
          err => {
            alert("Unable to take photo");
          }
        );
    };
    let errorCallback = e => {
      console.error(e);
      this.fileInput.nativeElement.click();
    };

    this.diagnostic
      .isCameraAvailable()
      .then(successCallback)
      .catch(errorCallback);
    // };
  }

  processWebImage(event) {
    console.log("evnet", event);
    let reader = new FileReader();
    reader.onload = readerEvent => {
      let imageData = (readerEvent.target as any).result;
      console.log("evnet", imageData);
      this.form.patchValue({
        profilePic: {
          base64: imageData,
          type: event.target.files[0].type.split("/")[1],
          name: new Date().getTime() + "-" + event.target.files[0].name
        }
      });
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return "url(" + this.form.controls["profilePic"].value.base64 + ")";
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) {
      return;
    }
    this.viewCtrl.dismiss(this.form.value);
  }
}
