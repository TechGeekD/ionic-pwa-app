import { Component, NgZone } from "@angular/core";
import { IonicPage, ModalController, NavController } from "ionic-angular";

import { Item } from "../../models/item";
import { Items } from "../../providers/providers";
import { FileTransfer } from "../../providers/providers";

@IonicPage()
@Component({
  selector: "page-list-master",
  templateUrl: "list-master.html"
})
export class ListMasterPage {
  currentItems: Item[];
  // currentItems: any;

  constructor(
    public navCtrl: NavController,
    private ngZone: NgZone,
    public items: Items,
    private fileTransfer: FileTransfer,
    public modalCtrl: ModalController
  ) {}

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
    this.items
      .get("users")
      .then(res => {
        this.currentItems = this.items.query();
      })
      .catch(err => {
        console.log("ErrorFetchListData", err);
      });
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    let addModal = this.modalCtrl.create("ItemCreatePage");
    addModal.onDidDismiss(item => {
      if (item) {
        let media = {
          name: item.profilePic.name,
          base64: item.profilePic.base64,
          type: item.profilePic.type
        };
        this.fileTransfer
          .upload(media)
          .then(downloadUrl => {
            item.profilePicName = item.profilePic.name;
            item.profilePic = downloadUrl;
            console.log(item);
            this.items.add([item]).then(resp => {
              console.log("form", resp);
              this.ngZone.run(() => {
                this.currentItems = this.items.query();
              });
            });
          })
          .catch(err => {
            console.log(err);
          });
      }
    });
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.items
      .delete(item)
      .then(res => {
        this.items
          .get("users")
          .then(res => {
            this.currentItems = this.items.query();
          })
          .catch(err => {
            console.log(err);
          });
      })
      .catch(err => {
        console.log(err);
      });
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push("ItemDetailPage", {
      item: item
    });
  }
}
