import { NgModule } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { IonicPageModule } from "ionic-angular";

import { Items } from "../../providers/providers";

import { ItemDetailPage } from "./item-detail";

@NgModule({
  declarations: [ItemDetailPage],
  imports: [
    IonicPageModule.forChild(ItemDetailPage),
    TranslateModule.forChild()
  ],
  exports: [ItemDetailPage],
  providers: [Items]
})
export class ItemDetailPageModule {}
