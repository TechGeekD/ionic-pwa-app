import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";

import { FirstRunPage, MainPage } from "../pages";

@IonicPage()
@Component({
  selector: "page-splash",
  templateUrl: "splash.html"
})
export class SplashPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private afAuth: AngularFireAuth,
    private viewCtrl: ViewController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad SplashPage");
    this.afAuth.authState.subscribe(
      res => {
        console.log("app", res);
        if (res !== null) {
          this.closeModal(MainPage);
        } else {
          this.closeModal(FirstRunPage);
        }
      },
      err => {
        this.viewCtrl.dismiss("err");
        console.log("authError", err);
      }
    );
  }

  closeModal(page) {
    setTimeout(() => {
      this.viewCtrl.dismiss(page);
    }, 3000);
  }
}
