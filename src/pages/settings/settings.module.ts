import { NgModule } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { IonicPageModule } from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";

import { SettingsPage } from "./settings";

@NgModule({
  declarations: [SettingsPage],
  imports: [IonicPageModule.forChild(SettingsPage), TranslateModule.forChild()],
  exports: [SettingsPage],
  providers: [Geolocation]
})
export class SettingsPageModule {}
