import { Component, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Events,
  AlertController
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { LoadingController } from "ionic-angular";

import { Settings, User } from "../../providers/providers";

/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
declare var google;

@IonicPage()
@Component({
  selector: "page-settings",
  templateUrl: "settings.html"
})
export class SettingsPage {
  @ViewChild("mapCanvas") mapElement: ElementRef;
  map: any;

  // Our local settings object
  options: any;

  settingsReady = false;

  form: FormGroup;

  profileSettings = {
    page: "profile",
    pageTitleKey: "SETTINGS_PAGE_PROFILE"
  };

  mapViewSettings = {
    page: "mapView",
    pageTitleKey: "SETTINGS_PAGE_MAP_VIEW"
  };

  page: string = "main";
  pageTitleKey: string = "SETTINGS_TITLE";
  pageTitle: string;

  subSettings: any = SettingsPage;

  constructor(
    public navCtrl: NavController,
    public settings: Settings,
    public formBuilder: FormBuilder,
    public navParams: NavParams,
    public translate: TranslateService,
    private user: User,
    private events: Events,
    private geolocation: Geolocation,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController
  ) {}

  _buildForm() {
    let group: any = {
      option1: [this.options.option1],
      option2: [this.options.option2],
      option3: [this.options.option3]
    };

    switch (this.page) {
      case "main":
        break;
      case "profile":
        group = {
          option4: [this.options.option4]
        };
        break;
    }
    this.form = this.formBuilder.group(group);

    // Watch the form for changes, and
    this.form.valueChanges.subscribe(v => {
      this.settings.merge(this.form.value);
    });
  }

  fetchUserLocationCoords() {
    this.geolocation
      .getCurrentPosition()
      .then(resp => {
        // resp.coords.latitude
        // resp.coords.longitude
        console.log("currentPos", resp);
      })
      .catch(error => {
        console.log("Error getting location", error);
      });

    let watch = this.geolocation.watchPosition();
    watch.subscribe(data => {
      console.log("wactch", data);
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
    });
  }

  loadMap() {
    let loadingContent = "Loading Chart";
    let loader = this.loadingCtrl.create({
      spinner: "dots",
      content: loadingContent
    });
    loader.present();
    this.geolocation.getCurrentPosition().then(
      position => {
        let latLng = new google.maps.LatLng(
          position.coords.latitude,
          position.coords.longitude
        );

        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(
          this.mapElement.nativeElement,
          mapOptions
        );

        this.addMarker();
        console.log("maps", position, this.map);
        loader.dismiss();
      },
      err => {
        loader.setContent("Failed To Load Chart");
        setTimeout(() => {
          loader.dismiss();
        }, 2000);
        console.log(err);
      }
    );
  }

  addMarker() {
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter()
    });

    let content = "<h4>Information!</h4>";

    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, "click", () => {
      infoWindow.open(this.map, marker);
    });
  }

  singOut() {
    this.user
      .signOut()
      .then(resp => {
        this.events.publish("user:logout");
      })
      .catch(err => {});
  }

  ionViewDidLoad() {
    // Build an empty form for the template to render
    this.form = this.formBuilder.group({});
  }

  ionViewWillEnter() {
    // Build an empty form for the template to render
    this.form = this.formBuilder.group({});

    this.page = this.navParams.get("page") || this.page;
    this.pageTitleKey = this.navParams.get("pageTitleKey") || this.pageTitleKey;

    this.translate.get(this.pageTitleKey).subscribe(res => {
      this.pageTitle = res;
    });

    this.settings.load().then(() => {
      this.settingsReady = true;
      this.options = this.settings.allSettings;

      this._buildForm();
      if (this.page == "mapView") {
        setTimeout(this.loadMap.bind(this), 1000);
      }
    });
  }

  ngOnChanges() {
    console.log("Ng All Changes");
  }

  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: "Logging Out",
      message: "Are you sure you want to logout?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            this.singOut();
          }
        }
      ]
    });
    confirm.present();
  }
}
