import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { LoadingController } from "ionic-angular";
import * as firebase from "firebase/app";
import "firebase/storage";

@Injectable()
export class FileTransfer {
  storageRef = firebase.storage().ref();

  constructor(public http: HttpClient, private loadingCtrl: LoadingController) {
    console.log("Hello UploadProvider Provider");
  }

  async upload(media) {
    let fileName = "images/" + media.name;
    let uploadTask = this.storageRef
      .child(fileName)
      .putString(media.base64, "data_url");

    let downloadUrl = await new Promise((resolve, reject) => {
      let loadingContent = "Upload is 0% done";
      let loader = this.loadingCtrl.create({
        spinner: "dots",
        content: loadingContent
      });
      loader.present();
      uploadTask.on(
        "state_changed",
        snapshot => {
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          let progress =
            snapshot["bytesTransferred"] / snapshot["totalBytes"] * 100;
          console.log("Upload is " + progress + "% done");
          loadingContent = "Upload File: " + Math.round(progress) + "% done";
          loader.setContent(loadingContent);
          switch (snapshot["state"]) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'
              console.log("Upload is paused");
              break;
            case firebase.storage.TaskState.RUNNING: // or 'running'
              console.log("Upload is running");
              break;
          }
        },
        error => {
          // Handle unsuccessful uploads
          switch (error["code"]) {
            case "storage/unauthorized":
              // User doesn't have permission to access the object
              break;
            case "storage/canceled":
              // User canceled the upload
              break;
            case "storage/unknown":
              // Unknown error occurred, inspect error.serverResponse
              break;
          }
          reject(error);
        },
        () => {
          loadingContent = "Upload File: Completed";
          loader.setContent(loadingContent);
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          let downloadURL = uploadTask.snapshot.downloadURL;
          // downloadUrl = downloadURL;
          console.log("download", downloadURL);
          setTimeout(() => {
            loader.dismiss();
          }, 1000);
          resolve(downloadURL);
          // return downloadURL;
        }
      );
    });
    return downloadUrl;
  }

  async delete(media) {
    let imageRef = this.storageRef.child("images/" + media.profilePicName);
    return await imageRef.delete();
  }
}
