import { Platform } from "ionic-angular";
import { Injectable } from "@angular/core";

import "rxjs/add/operator/toPromise";

import { Api } from "../api/api";

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class User {
  _user: any;

  constructor(public api: Api, private platform: Platform) {}

  async loginWithProvider() {
    let user;
    let token;
    if (this.platform.is("cordova")) {
      user = await this.api.nativeGoogleLogin();
    } else {
      await this.api
        .signInWithGoogle()
        .then(result => {
          // This gives you a Google Access Token. You can use it to access the Google API.
          token = result.credential.accessToken;
          // The signed-in user info.
          user = result.user;
          this._loggedIn(result);
        })
        .catch(error => {
          // Handle Errors here.
          let errorCode = error.code;
          let errorMessage = error.message;
          // The email of the user's account used.
          let email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          let credential = error.credential;
          throw new Error(error);
          // ...
        });
    }
    return user;
  }

  async signOut() {
    return await new Promise((resolve, reject) => {
      this.api.signOut().then(
        resp => {
          this._loggedIn(resp);
          resolve(resp);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  async normalLogin(userDetails) {
    return await this.api.normalSignIn({
      email: userDetails.email,
      password: userDetails.password
    });
  }

  async normalSignup(userDetails) {
    return await this.api.normalSignup({
      email: userDetails.email,
      password: userDetails.password
    });
  }

  async phoneNumberSignIn() {
    // with phoneNumber to be added here
  }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    let seq = this.api.post("login", accountInfo).share();

    seq.subscribe(
      (res: any) => {
        // If the API returned a successful response, mark the user as logged in
        if (res.status == "success") {
          this._loggedIn(res);
        } else {
        }
      },
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    let seq = this.api.post("signup", accountInfo).share();

    seq.subscribe(
      (res: any) => {
        // If the API returned a successful response, mark the user as logged in
        if (res.status == "success") {
          this._loggedIn(res);
        }
      },
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this._user = null;
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    this._user =
      typeof resp === "object" ? ("user" in resp ? resp.user : null) : null;
    console.log("make", this._user);
  }
}
