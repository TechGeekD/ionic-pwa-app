import { GooglePlus } from "@ionic-native/google-plus";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { AngularFireAuth } from "angularfire2/auth";
import { AngularFirestore } from "angularfire2/firestore";
import * as firebase from "firebase/app";
import AuthProvider = firebase.auth.AuthProvider;
import "rxjs/add/operator/map";

// import "firebase/firestore";

import { Item } from "../../models/item";
import { FileTransfer } from '../fileTransfer/fileTransfer';

export interface _Item {
  name?: string;
  profilePic?: string;
  about?: string;
}

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  url: string = "https://example.com/api/v1";

  constructor(
    public http: HttpClient,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private gplus: GooglePlus,
    private fileTransfer: FileTransfer
  ) {
    // afs.collection("users");
    firebase.auth().useDeviceLanguage();
    this.checkAuthState();
  }

  async _add(collection: string, params: Item) {
    let docs = this.afs.collection(collection);
    params.forEach((element, index) => {
      docs
        .add(element)
        .then(docRef => {
          docs.doc(docRef.id).update({
            id: docRef.id
          });
          console.log("id", docRef.id);
        })
        .catch(err => {
          console.error(err);
        });
    });
  }

  _get(collection: string, params?: any) {
    let docs = this.afs.collection(collection);
    return docs.snapshotChanges().map(actions => {
      console.log("actions", actions);
      return actions.map(a => {
        const data = a.payload.doc.data() as _Item;
        return { ...data };

        // const _id = a.payload.doc.id; // if no id pushed to data fetch it this way & return
        // return { _id, ...data };
      });
    });
  }

  async _update(collection: string, params?: any) {
    let docs = this.afs.collection(collection);
    return await new Promise((resolve, reject) => {
      docs
        .doc(params.id)
        .update(params)
        .then(() => {
          console.log("updated");
          resolve(true);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  async _delete(collection: string, params?: any) {
    console.log(params);
    let docs = this.afs.collection(collection);
    return await new Promise((resolve, reject) => {
      docs
        .doc(params.id)
        .delete()
        .then(() => {
          this.fileTransfer
            .delete(params)
            .then(resp => {
              console.log("FileDeleted", resp);
            })
            .catch(error => {
              console.error("FileDeleteError", error);
            });
          resolve(true);
          console.log("deleted");
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  get(endpoint: string, params?: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }

    return this.http.get(this.url + "/" + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    return this.http.post(this.url + "/" + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + "/" + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + "/" + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.url + "/" + endpoint, body, reqOpts);
  }

  // firebase
  // signInWithGoogle() {
  //   console.log("Sign in with google");
  //   return this.oauthSignIn(new firebase.auth.GoogleAuthProvider());
  // }

  // private oauthSignIn(provider: AuthProvider) {
  //   if (!(<any>window).cordova) {
  //     return this.afAuth.auth.signInWithPopup(provider);
  //   } else {
  //     return this.afAuth.auth.signInWithRedirect(provider).then(() => {
  //       return this.afAuth.auth
  //         .getRedirectResult()
  //         .then(result => {
  //           // This gives you a Google Access Token.
  //           // You can use it to access the Google API.
  //           let token = result.credential.accessToken;
  //           // The signed-in user info.
  //           let user = result.user;
  //           console.log(token, user);
  //         })
  //         .catch(function(error) {
  //           // Handle Errors here.
  //           alert(error.message);
  //         });
  //     });
  //   }
  // }

  checkAuthState() {
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var isAnonymous = user.isAnonymous;
        var uid = user.uid;
        var providerData = user.providerData;
        console.log("authState", user);
        // ...
      } else {
        console.log("authState", user);
        // User is signed out.
        // ...
      }
    });
  }

  async normalSignup({ email, password }) {
    return await new Promise((resolve, reject) => {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(resp => {
          resolve(resp);
        })
        .catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log("Signup", error);
          reject(error);
          // ...
        });
    });
  }

  async normalSignIn({ email, password }) {
    console.log(email, password);
    return await new Promise((resolve, reject) => {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(resp => {
          console.log(resp);
          resolve(resp);
        })
        .catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log("SignIn", error);
          reject(error);
          // ...
        });
    });
  }

  async nativeGoogleLogin(): Promise<void> {
    try {
      const gplusUser = await this.gplus.login({
        webClientId:
          "345263253026-6kjrj8r6mb90gearjf28d9cqquldihc1.apps.googleusercontent.com",
        offline: true,
        scopes: "profile email"
      });

      return await this.afAuth.auth.signInWithCredential(
        firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken)
      );
    } catch (err) {
      console.log(err);
    }
  }

  async signInWithGoogle() {
    return await this.afAuth.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    );
  }

  async signInWithPhoneNumber() {
    // with phoneNumber to be added here
  }

  signOut() {
    return this.afAuth.auth.signOut();
  }
}
