import { Injectable } from "@angular/core";

import { Item } from "../../models/item";
import { Api } from "../api/api";

@Injectable()
export class Items {
  items: Item[] = [];
  // items: Observable<any[]>;
  // items: Observable<Item>;
  defaultItem: any = {
    name: "Burt Bear",
    profilePic: "assets/img/speakers/bear.jpg",
    about: "Burt is a Bear."
  };

  constructor(public api: Api) {
    this.get("users");
  }

  query(params?: any) {
    if (!params) {
      return this.items ? this.items : [this.defaultItem];
    }

    return this.items.filter(item => {
      for (let key in params) {
        let field = item[key];
        if (
          typeof field == "string" &&
          field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0
        ) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  async get(collection: string) {
    await this.api._get(collection).subscribe(
      docItems => {
        if (this.items.length > 0) {
          this.items = [];
        }
        console.log("docItems", docItems);
        docItems.forEach(user => {
          this.items.push(new Item(user));
        });
      },
      err => {
        console.error("get " + collection, err);
      }
    );
  }

  async add(item: Item) {
    this.api._add("users", item);
  }

  delete(item: Item) {
    return this.api._delete("users", item);
  }
}
