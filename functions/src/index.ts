// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
import * as functions from "firebase-functions";
// The Firebase Admin SDK to access the Firebase Realtime Database.
import * as admin from "firebase-admin";

admin.initializeApp(functions.config().firebase);

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   response.send("Hello from Firebase!");
// });

// Take the text parameter passed to this HTTP endpoint and insert it into the
// Realtime Database under the path /messages/:pushId/original
exports.addMessage = functions.https.onRequest((req, res) => {
  // Grab the text parameter.
  const original = req.query.text;
  // Push the new message into the Realtime Database using the Firebase Admin SDK.
  return admin
    .database()
    .ref("/messages")
    .push({ original: original })
    .then(snapshot => {
      // Redirect with 303 SEE OTHER to the URL of the pushed object in the Firebase console.
      return res.redirect(303, snapshot.ref);
    });
});

// Listens for new messages added to /messages/:pushId/original and creates an
// uppercase version of the message to /messages/:pushId/uppercase
exports.makeUppercase = functions.database
  .ref("/messages/{pushId}/original")
  .onWrite(event => {
    // Grab the current value of what was written to the Realtime Database.
    const original = event.data.val();
    console.log("Uppercasing", event.params.pushId, original);
    const uppercase = original.toUpperCase();
    // You must return a Promise when performing asynchronous tasks inside a Functions such as
    // writing to the Firebase Realtime Database.
    // Setting an "uppercase" sibling in the Realtime Database returns a Promise.
    return event.data.ref.parent.child("uppercase").set(uppercase);
  });

exports.createUser = functions.firestore
  .document("users/{userId}")
  .onCreate(event => {
    // Get an object representing the document
    // e.g. {'name': 'Marie', 'age': 66}
    const newValue = event.data.data();

    // access a particular field as you would any JS property
    const name = newValue.name;
    console.log("createUser", newValue, name);
    // perform desired operations ...
  });

exports.updateUser = functions.firestore
  .document("users/{userId}")
  .onUpdate(event => {
    // Get an object representing the document
    // e.g. {'name': 'Marie', 'age': 66}
    const newValue = event.data.data();

    // ...or the previous value before this update
    const previousValue = event.data.previous.data();

    // access a particular field as you would any JS property
    const name = newValue.name;
    console.log("updateUser", newValue, previousValue, name);
    // perform desired operations ...
  });

exports.deleteUser = functions.firestore
  .document("users/{userID}")
  .onDelete(event => {
    // Get an object representing the document prior to deletion
    // e.g. {'name': 'Marie', 'age': 66}
    const deletedValue = event.data.previous.data();

    console.log("deleteUser", deletedValue);
    // perform desired operations ...
  });

exports.modifyUser = functions.firestore
  .document("users/{userID}")
  .onWrite(event => {
    // Get an object with the current document value.
    // If the document does not exist, it has been deleted.
    const document = event.data.exists ? event.data.data() : null;

    // Get an object with the previous document value (for update or delete)
    const oldDocument = event.data.previous.data();

    console.log("modifyUser", document, oldDocument);
    // perform desired operations ...
  });

exports.sendWelcomeEmail = functions.auth.user().onCreate(event => {
  console.log("endWelcomeEmail", event);
});

exports.sendByeEmail = functions.auth.user().onDelete(event => {
  console.log("sendByeEmail", event);
});
